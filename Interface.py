import tkinter as tk
from tkinter import filedialog
import Prediction

root = tk.Tk()

canvas1 = tk.Canvas(root, width=600, height=800)
canvas1.pack()


def hello():
    folder_selected = filedialog.askdirectory()
    label1 = tk.Label(root, text='Начинаем расчет', fg='green', font=('helvetica', 12, 'bold'))
    canvas1.create_window(220, 300, window=label1)
    df_result = Prediction.make_predictions(folder_selected)
    label1["text"] = df_result.head(15)


button1 = tk.Button(text='Выбрать папку с файлами для классификации', command=hello, bg='brown', fg='white')
canvas1.create_window(300, 50, window=button1)

root.mainloop()
