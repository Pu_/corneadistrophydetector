import os
from PIL import Image

IMG_CROP = (110, 60, 355, 520)  # Координаты углов окна для обрезания

CATEGORIES = ['pathology', 'norm']
sets = ["raw_data"]

# Создание массива с данными для обучения
train = []

for dataset in sets:
    for cat in CATEGORIES:
        label = CATEGORIES.index(cat)
        path1 = os.path.join("old_data", dataset, cat)  # Получение фотографий из двух папок разных категорий
        train_names = os.listdir(path1)
        # train_names_jpg = filter(lambda x: x.endswith('.jpg'), train_names)  # Выделение нужных файлов
        for i in range(len(train_names)):
            img = Image.open(path1 + '/' + train_names[i])
            img = img.crop(IMG_CROP)  # Обрезание фотографии по заданным координатам
            img.save("old_data/processed/" + cat + "_" + str(i) + '.jpg')
            # crop = img_to_array(img)  # Перевод фотографий в массив чисел
            # train.append([crop, label])  # Добавление массива чисел и меток в массив для обучения
