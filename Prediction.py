import os
import torch
from PIL import Image
import torch.nn as nn
from pathlib import Path
# noinspection PyProtectedMember
from torch.utils.data import Dataset, DataLoader
from sklearn.preprocessing import LabelEncoder
import pickle
from torchvision import transforms
import numpy as np
import pandas as pd


class CorneaDataset(Dataset):
    """
    Датасет с картинками, который паралельно подгружает их из папок
    производит скалирование и превращение в торчевые тензоры
    """

    def __init__(self, files, mode):
        super().__init__()
        # список файлов для загрузки
        self.files = sorted(files)
        # режим работы
        self.mode = mode

        if self.mode not in DATA_MODES:
            print(f"{self.mode} is not correct; correct modes: {DATA_MODES}")
            raise NameError

        self.len_ = len(self.files)

        self.label_encoder = LabelEncoder()

        if self.mode != 'test':
            self.labels = [path.parent.name for path in self.files]
            self.label_encoder.fit(self.labels)

            with open('label_encoder.pkl', 'wb') as le_dump_file:
                pickle.dump(self.label_encoder, le_dump_file)

    def __len__(self):
        return self.len_

    def __getitem__(self, index):
        # Я пробовал много разных методов аугментации, но лучший результат получился при использовании только
        # Random Horizontal Flip
        transform1 = transforms.Compose([
            transforms.ColorJitter(brightness=0.25),
            transforms.ColorJitter(contrast=0.25),
            transforms.ColorJitter(saturation=0.25),
            transforms.ColorJitter(hue=0.25),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ])
        x = load_sample(self.files[index])
        # x = _prepare_sample(x)
        x = transform1(x)
        x = x / 255

        if self.mode == 'test':
            return x
        else:
            label1 = self.labels[index]
            label_id = self.label_encoder.transform([label1])
            y = label_id.item()
            return x, y


train_on_gpu = torch.cuda.is_available()
if not train_on_gpu:
    print('CUDA is not available.  Training on CPU ...')
    DEVICE = torch.device("cpu")
else:
    print('CUDA is available!  Training on GPU ...')
    DEVICE = torch.device("cuda")

path2 = "images"
IMG_CROP = (110, 60, 355, 520)
DATA_MODES = ['train', 'val', 'test']


def load_sample(file):
    image = Image.open(file)
    image.load()
    return image


def predict(model1, test_loader1):
    with torch.no_grad():
        logits = []

        for inputs in test_loader1:
            inputs = inputs.to(DEVICE)
            model1.eval()
            outputs = model1(inputs).cpu()
            logits.append(outputs)

    probs1 = nn.functional.softmax(torch.cat(logits), dim=-1).numpy()
    return probs1


def make_predictions(folder_selected):
    # Создание массива с данными для обучения
    image_names = os.listdir(folder_selected)
    # if os.path.exists("images"):
    #     os.remove("images")
    if not os.path.exists("images"):
        os.mkdir("images")
    for i in range(len(image_names)):
        img = Image.open(folder_selected + '/' + image_names[i])
        img = img.crop(IMG_CROP)  # Обрезание фотографии по заданным координатам
        img.save("images/" + image_names[i])

    # selected_model = torch.load("models/resnet18")
    selected_model = torch.load("models/resnet18", map_location={'cuda:0': 'cpu'})
    selected_model.to(DEVICE)
    selected_model.eval()
    label_encoder = pickle.load(open("label_encoder.pkl", 'rb'))
    test_files = sorted(list(Path(path2).rglob('*.jpg')))
    test_dataset = CorneaDataset(test_files, mode="test")
    test_loader = DataLoader(test_dataset, shuffle=False, batch_size=64)
    probs = predict(selected_model, test_loader)
    preds = label_encoder.inverse_transform(np.argmax(probs, axis=1))
    test_filenames = [path1.name for path1 in test_dataset.files]

    df_result = pd.DataFrame({'Файл': test_filenames, 'Класс': preds})
    df_result.head()
    df_result.to_csv('Результат предсказаний.csv', index=True)
    print("Prediction completed!")
    return df_result
